# BillionSeqViz

This project aims to develop a scalable software tool for visually exploring and validating the clustering structure of billions of sequences in biomedical research.

The two components (sub-projects) are sequence condensation tree (SCT) and SCT and t-SNE based visual exploration system. 